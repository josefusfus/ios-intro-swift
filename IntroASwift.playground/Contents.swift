/*:
 # Intro a Swift
 
 */
import Foundation


//: ## variables y constantes
var size : Float = 42.0
var answer = 42
let name = "Anakin"

//: ## Todo es un objeto
Int.max
Double.abs(-42.0)


//: ## conversiones
let a = Int(size)
let ans = String(answer)

//: ## typealias: sirve para otros nombres a un tipo
typealias Integer = Int

let a1 : Integer = 42

//: ## Colecciones
var swift = "Nuevo lenguaje de Apple"
swift = swift + "!"

var words = ["uno", "dos", "tres", "cuatro"]
words[0]

let numberNames = [1: "one", 2: "two"]
numberNames[2]

//: ## Iterar
var total = ""
for element in [1,2,3,4,5,5,6,7,7]{
    total = "\(total) \(element)"
}
print(total)

for (key, value) in numberNames{
    print("\(key) -- \(value)")
}

// Tuple
let pair = (1, "one")
pair.0
pair.1


for i in 1...5{
    print(i)
}

for i in 1..<5{
    print(i)
}


//: ## Funciones

func h(perico a:Int, deLosPalotes b:Int) -> Int{
    return (a + b) * a
}

h(perico: 3, deLosPalotes: 5)

// función sin nombres externos:
// la variable anónima _
func f(a:Int, _ b: Int) -> Int{
    return (a + b)
}

f(3,4)


func sum(a:Int, _ b:Int, thenMultiplyBy c:Int)->Int{
    
    return (a+b) * c
}

sum(3, 4, thenMultiplyBy: 5)

// default values
func addSuffixTo(a:String, suffix:String = "ingly")->String{
    
    return a + suffix
}

addSuffixTo("accord")
addSuffixTo("Objective-", suffix: "C")

// Return values
func namesofNumbers(a:Int) -> (Int, String, String){
    
    var val: (Int, String, String)
    
    switch a {
    case 1:
        val = (1, "one", "uno")
    case 2:
        val = (2, "two", "dos")
    default:
        val = (a, "Go check Google translator", "vete a google" )
    }
    return val
}

let r = namesofNumbers(2)
let (_, en, es) = namesofNumbers(1)

en
es
(es, en)

//: ## Funciones de alto nivel

typealias IntToIntFunc = (Int)->Int
var z : IntToIntFunc

// Funciones como parámetros
func apply(f: IntToIntFunc, n: Int)->Int{
    return f(n)
}

func doubler(a:Int)->Int{
    return a * 2
}

func add42(a:Int) ->Int{
    return a + 42
}

apply(doubler, n: 4)


// Funciones como valores de retorno

func compose(f: IntToIntFunc,
             _ h: IntToIntFunc) -> IntToIntFunc{
    
    // funciones dentro de funciones??
    func comp(a:Int) -> Int{
        return f(h(a))
    }
    return comp
}

compose(add42, doubler)(8)

let comp = compose(add42, doubler)

// Funciones de un mismo tipo, en un array
let funcs = [add42, doubler, comp]
for f in funcs{
    f(33)
}

//: ## Closure Syntax (representación literal de funciones)

func g(a:Int)->Int{
    return a + 42
}
// exactamente igual a...
let gg = {(a:Int) ->Int in
        return a + 42
}

g(1)
gg(1)


// sintaxis simplificada de clausuras
let closures = [g,
                {(a:Int)->Int in return a - 42},
                {a in return a + 45},
                {a in a / 42},
                {$0 * 42}
]

//: Operadores: son clausuras

typealias BinaryFunc = (Int,Int)->Int
let applier = {(f: BinaryFunc, m:Int, n:Int) ->Int
                    in
                    return f(m,n)}

applier(*, 2, 4)


// Trailing closure
func applierInv(m:Int, _ n:Int, f:BinaryFunc)->Int{
    return applier(f,m,n)
}

let c = applierInv(2, 4, f: {$0 * 2 + $1 * 3})

// 100% equivalent a:
let cc = applierInv(2, 4){
    return $0 * 2 + $1 * 3
}











